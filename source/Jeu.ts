//==================================================================================================
// ANIMATION AVEC TYPESCRIPT                                                                 Jeu.ts
//==================================================================================================

// Classe  J e u //---------------------------------------------------------------------------------
class Jeu extends Scene {
 //----------------------------------------------------------------------------------------Attributs
 /* Declarer ici les attributs de la scene. */
 public carte_ : Array<Array<number>>;
 public wall_ : Sprite;
 public pas_ : number;
 public pacman_ : Pacman;
 public noisette_ : Sprite;
 public pastilles_ : Array<Array<Sprite>>;



 //-------------------------------------------------------------------------------------Constructeur
 public constructor(element : HTMLElement) {
  super(element,false);
  /* Ecrire ici le code qui initialise la scene. */
  this.pas_ = 32; 
  this.pastilles_ = [];
 }


private initialiserCarte(){
  this.carte_ = [];
  this.carte_[0] = [1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1];
  this.carte_[1] = [1 ,10,2 ,2 ,2 ,1 ,10,2 ,2 ,1];
  this.carte_[2] = [1 ,1 ,2 ,1 ,1 ,1 ,2 ,1 ,2 ,1];
  this.carte_[3] = [1 ,2 ,2 ,2 ,1 ,1 ,2 ,1 ,1 ,1];
  this.carte_[4] = [1 ,2 ,1 ,1 ,2 ,2 ,2 ,1 ,2 ,9];
  this.carte_[5] = [1 ,2 ,2 ,2 ,2 ,1 ,2 ,1 ,2 ,1];
  this.carte_[6] = [1 ,2 ,1 ,8 ,1 ,1 ,2 ,2 ,2 ,1];
  this.carte_[7] = [1 ,2 ,1 ,2 ,1 ,1 ,2 ,1 ,1 ,1];
  this.carte_[8] = [1 ,2 ,2 ,2 ,1 ,2 ,2 ,2 ,2 ,1];
  this.carte_[9] = [1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1];
}

private dessinerLabyrinthe(){
  for(let i = 0 ; i<this.carte_.length; i++){
    this.pastilles_[i] = [];
    for(let j = 0 ; j<this.carte_[i].length; j++){
      if(this.carte_[i][j] == 1){
        this.wall_ = new Sprite(document.createElement("img"));
        this.wall_.setImage("mur.jpg",32,32);
        this.wall_.setXY(this.pas_*j,this.pas_*i);
        this.appendChild(this.wall_);
      }
      if (this.carte_[i][j] == 8){
        this.pacman_ = new Pacman(this,document.createElement("img"),i,j);
        this.pacman_.setImage("Squirrel.png",32,32);
        this.pacman_.setXY(this.pas_*j,this.pas_*i);
        this.appendChild(this.pacman_);
      }
      if (this.carte_[i][j] == 2){
        this.noisette_ = new Sprite(document.createElement("img"));
        this.noisette_.setImage("noisette.png",32,32);
        this.noisette_.setXY(this.pas_*j,this.pas_*i);
        this.appendChild(this.noisette_);
      }
      if(this.carte_[i][j] == 2){
        this.pastilles_[i][j] = this.noisette_;
      } else {
        this.pastilles_[i][j] = null;
      }
    }
  }console.log(this.pastilles_);
}

public retirePastille(){
  
}
 //--------------------------------------------------------------------------------------------start
 public override start() {
  /* Ecrire ici le code qui demarre la scene. */
  console.log(this.carte_);
  this.initialiserCarte();
  this.dessinerLabyrinthe();
  this.pacman_.animer();
  this.pacman_.estArriver();
 }

 //--------------------------------------------------------------------------------------------pause
 public override pause() {
  /* Ecrire ici le code qui met la scene en pause. */
  this.pacman_.figer();
 }

 //------------------------------------------------------------------------------------------unpause
 public override unpause() {
  /* Ecrire ici le code qui sort la scene de la pause. */
 }

 //--------------------------------------------------------------------------------------------clean
 public override clean() {
  /* Ecrire ici le code qui nettoie la scene en vue d'un redemarrage. */
 }
}

// Fin //-------------------------------------------------------------------------------------------

class Pacman extends Sprite {

  //Attributs
  public scene_ : Jeu;
  private ecouteurDeplacer : any;
  public px_ : number;
  public py_ : number;
  //Constructeur
  public constructor(scene : Jeu,element : HTMLElement, py : number, px : number){
    super(element);
    this.scene_ = scene;
    this.px_ = px;
    this.py_ = py;
    this.ecouteurDeplacer = (event : KeyboardEvent) => {this.deplacer(event)};
    this.getElement().style.zIndex = "2";
  }

  public haut(){
    if(this.scene_.carte_[this.py_-1][this.px_] == 1){
      
    }else{
      this.py_ = this.py_-1;
      this.setXY(this.getX(),this.getY()-this.scene_.pas_);
      this.getElement().style.transform = 'rotate(-90deg)';
      // this.setXY(this.px_*this.scene_.pas_,this.py_*this.scene_.pas_);
    }
  }
  public bas(){
    if(this.scene_.carte_[this.py_+1][this.px_] == 1){
      
    }else{
      this.py_ = this.py_+1;
      this.setXY(this.getX(),this.getY()+this.scene_.pas_);
      this.getElement().style.transform = 'rotate(90deg)';
      // this.setXY(this.px_*this.scene_.pas_,this.py_*this.scene_.pas_);
    }
  }
  public gauche(){
    if(this.scene_.carte_[this.py_][this.px_-1] == 1){
      
    }else{
      this.px_ = this.px_-1;
      this.setXY(this.getX()-this.scene_.pas_,this.getY());
      this.getElement().style.transform = 'rotate(90deg) scale(-1,1)';
      // this.setXY(this.px_*this.scene_.pas_,this.py_*this.scene_.pas_);
    }
  }
  public droite(){
    if(this.scene_.carte_[this.py_][this.px_+1] == 1){
      
    }else{
      this.px_ = this.px_+1;
      this.setXY(this.getX()+this.scene_.pas_,this.getY());
      this.getElement().style.transform = 'rotate(90deg) scale(1,1)';
      // this.setXY(this.px_*this.scene_.pas_,this.py_*this.scene_.pas_);
    }
  }
  private deplacer(event : KeyboardEvent){
    if(this.estArriver() == false){
      if(event.key == "ArrowLeft"){
      this.gauche();
      }
      if(event.key == "ArrowRight"){
        this.droite();
      }
      if(event.key == "ArrowDown"){
        this.bas();
      }
      if(event.key == "ArrowUp"){
        this.haut();
      }
    }
    
  }

  public animer(){
    window.addEventListener("keydown", this.ecouteurDeplacer);
  }

  public figer(){
    window.removeEventListener("keydown", this.ecouteurDeplacer);
  }

  public estArriver(){
    return this.scene_.carte_[this.py_][this.px_] == 9;
  }
}